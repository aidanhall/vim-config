"Some basics:
	set nocompatible
	filetype plugin on
	set omnifunc=syntaxcomplete#Complete
	set number relativenumber
	set hlsearch
	set wildmode=longest,list,full
	set wildmenu
	let mapleader = " "
	set termwinsize=10x0
	set mouse=a
	set encoding=UTF-8
	set smartcase ignorecase
	set switchbuf="useopen, usetab, split"


" Status line:
    set laststatus=2 " Always on.
    "set statusline= " Initialise it, clear.
    "set statusline+=%F " Opened file.
    "set statusline+=\ %l
    "set statusline+=%=%l
    
    set statusline=\ \ %f%m%r%h%w\ %{&ff}\ %Y\ [0x\%02.2B]\ %#warningmsg#%{SyntasticStatuslineFlag()}%*\ %=%l/%L,%v\ %p%%\ 


" Indentation, tabs and spacing:
	set autoindent
	set cindent
	set tabstop=8
	set softtabstop=4 shiftwidth=4  noexpandtab

" Copy and paste:
"	vnoremap <C-c> "*y :let @+=@*<CR>
"	map <C-p> "+p

" Cursor shape for Konsole:
	let &t_SI = "\<Esc>]50;CursorShape=1\x7"
	let &t_SR = "\<Esc>]50;CursorShape=2\x7"
	let &t_EI = "\<Esc>]50;CursorShape=0\x7"

" Syntax highlighting:
	syntax on
	highlight Comment ctermfg=red term=italic term=bold
	highlight Constant ctermfg=magenta
	highlight Identifier ctermfg=cyan
	highlight Statement ctermfg=yellow
	highlight Type ctermfg=green
	highlight Error ctermbg=red ctermfg=black cterm=bold
	highlight LineNr ctermfg=darkcyan
	highlight Special ctermfg=red
	highlight Search ctermbg=darkgray
	highlight Visual ctermbg=gray
	highlight MatchParen ctermfg=black ctermbg=cyan
	highlight lspReference ctermfg=black ctermbg=green
	highlight SpellBad ctermbg=brown
	" Python:
		highlight pythonAttribute ctermfg=blue
	" CSharp:
		highlight csBraces ctermfg=lightmagenta
		highlight csUserIdentifier ctermfg=lightgreen
    " Syntastic:
        highlight SpellBad ctermfg=black ctermbg=darkred cterm=italic
	highlight SpellCap ctermbg=none cterm=undercurl

" This function returns the highlighting type of a term, making it easier to
" edit syntax highlighting for different languages.
nmap <leader>ht :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc


" Specify a directory for plugins 

call plug#begin('~/.vim/plugged')
 
" Any valid git URL is allowed for plugin
" Plug 'valid git URL'

" Shorthand notation for plugin
" Plug 'foo/bar'
Plug 'vimwiki/vimwiki' 
Plug 'Omnisharp/omnisharp-vim'
Plug 'tpope/vim-dispatch'
Plug 'Shougo/vimproc.vim'
Plug 'w0rp/ale'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf'
Plug 'tpope/vim-vinegar'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-syntastic/syntastic'
Plug 'plasticboy/vim-markdown'
Plug 'yami-beta/asyncomplete-omni.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim'
Plug 'ryanolsonx/vim-lsp-python'
"Plug 'scrooloose/nerdtree'
Plug 'artur-shaik/vim-javacomplete2'
Plug 'jdonaldson/vaxe'
Plug 'yami-beta/asyncomplete-omni.vim'

" Rust plugins:
Plug 'rust-lang/rust.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'racer-rust/vim-racer'
Plug 'keremc/asyncomplete-racer.vim'

" Initialize plugin system
call plug#end()

" Folds:
	set foldmethod=indent
	set foldenable
	set foldlevel=2


" Split navigation and behaviour:
	nnoremap <C-J> <C-W><C-J>
	nnoremap <C-K> <C-W><C-K>
	nnoremap <C-L> <C-W><C-L>
	nnoremap <C-H> <C-W><C-H>
	set splitbelow splitright

" Tab management:
	map <F9> :tabprevious <CR>
	map <F10> :tabnext <CR>
	map <leader>tn :tabnew <CR>
	map <leader>te :term <CR>
	map <leader>tx :put<space>=expand('%:p')<Enter>"td$:wq<Enter>
	map <leader>tp :tabnew<space>"tp<Enter>




" FZF:
	"map <F8> :FZF<Enter>
	
" Global Keybindings:
	nnoremap <F2> :term <CR>
	tnoremap <F2> exit <CR>
	map <F3> :nohlsearch <CR>
	map <F4> :call <SID>ToggleScrolloff()<CR>
	"map <F4> :setlocal spell! <CR>
	nnoremap <F12> :setlocal cursorline! <CR>
	nnoremap <F12> :setlocal spell! <CR>

function! <SID>ToggleScrolloff()
    if &scrolloff==0
	set scrolloff=9999
    else
	set scrolloff=0
    endif
endfunc

" IDE Functions:
	"map <leader><leader> <Esc>/<++><Enter>"_d4l
	"inoremap <C-Space> <C-x><C-o>

	" Universal commands:
	   nnoremap <F5> :terminal ++close /home/aidan/scripts/compile1.sh %:p<enter>
	   nnoremap <F6> :terminal ++close /home/aidan/scripts/compile2.sh %:p<enter>

	autocmd FileType vim nnoremap <F5> :so<space>%<Enter>

	" Syntastic:
		let g:syntastic_always_populate_loc_list = 1
		let g:syntastic_auto_loc_list = 1
		let g:syntastic_check_on_open = 1
		let g:syntastic_check_on_wq = 0
		let g:syntastic_python_checkers = ['bandit', 'python']
		let g:syntastic_java_checkers = ['checkstyle']
		let g:syntastic_cs_checkers = ['mcs']
		let g:syntastic_vim_checkers = ['vint']
		let g:syntastic_haxe_checkers = ['haxe']

	" asyncomplete:
	    
	    let g:asyncomplete_auto_popup = 1
	    inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
	    inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
	    inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
	    autocmd User asyncomplete_setup call asyncomplete#register_source(
	        \ asyncomplete#sources#racer#get_source_options())

	    call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
	    \ 'name': 'omni',
	    \ 'whitelist': ['*'],
	    \ 'blacklist': ['c', 'cpp', 'html', 'rust', 'nroff', 'haxe', 'vim'],
	    \ 'completor': function('asyncomplete#sources#omni#completor')
	    \  }))
	" html:
		"autocmd FileType html inoremap <leader>i <em></em><Space><++><Esc>FeT>i
		"autocmd FileType html inoremap <leader>b <strong></strong><Space><++><Esc>FgT>i
		"autocmd FileType html inoremap <leader>html <!doctype<Space>html><Enter><html><Enter><head><Enter><Tab><title><++></title><Enter></head><Enter><body><Enter><Tab><++><Enter></body><Enter></html><Esc>?<++><Enter>n"_c4l
		" Headings:
			"autocmd FileType html inoremap ;h1 <h1></h1><Space><++><Esc>F1T>i
			"autocmd FileType html inoremap ;h2 <h2></h2><Space><++><Esc>F2T>i
			"autocmd FileType html inoremap ;h3 <h3></h3><Space><++><Esc>F3T>i
			"autocmd FileType html inoremap ;h4 <h4></h4><Space><++><Esc>F4T>i
			"autocmd FileType html inoremap ;h5 <h5></h5><Space><++><Esc>F5T>i
			"autocmd FileType html inoremap ;h6 <h6></h6><Space><++><Esc>F6T>i

		" Opening the html file in a browser.
			"autocmd FileType html nnoremap <F5> :!firefox<Space>%:p<Enter><Enter>
			"autocmd FileType html nnoremap <F6> :!w3m<Space>%:p<Enter><Enter>
		" Lists:
			"autocmd FileType html inoremap ;ol <ol><Enter><Tab><li><++></li><++><Enter><Esc>hxi</ol><Esc>?<++><Enter>n"_c4l
			"autocmd FileType html inoremap ;ul <ul><Enter><Tab><li><++></li><++><Enter><Esc>hxi</ul><Esc>?<++><Enter>n"_c4l
			"autocmd FileType html inoremap ;li <li><++></li><++>?<++><Enter>"_c4l
		autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
	" Python:
		" Executes the program:
			"autocmd FileType python nnoremap <F5> <Esc>:w<Enter>:!python3<Space>%:p<Enter>

		if executable('pyls')
		        au User lsp_setup call lsp#register_server({
			        \ 'name': 'pyls',
			        \ 'cmd': {server_info->['pyls']},
			        \ 'whitelist': ['python'],
			        \ 'workspace_config': {'pyls': {'plugins': {'pydocstyle': {'enabled': v:true}}}}
			        \ })
		    endif

	
	" Haxe:
	    "autocmd filetype haxe set g:vaxe_cache_server = 1
	    "autocmd filetype haxe set omnifunc=
	    autocmd filetype haxe set autowrite
	" C#:
		" OmniSharp:
			"let g:OmniSharp_timeout = 5
			"set completeopt=longest,menuone,preview
			"set previewheight=5
			"let g:ale_linters = { 'cs': ['OmniSharp'] }
			"let g:ale_echo_cursor = 0
			"let g:OmniSharp_highlight_types = 0
""			let g:OmniSharp_server_stdio = 1
			"augroup omnisharp_commands
				"autocmd!
"
				"" When Syntastic is available but not ALE, automatic syntax check on events
				"" (TextChanged requires Vim 7.4)
				""autocmd BufEnter,TextChanged,InsertLeave *.cs SyntasticCheck
"
				"" Show type information automatically when the cursor stops moving
				"autocmd CursorHold *.cs call OmniSharp#TypeLookupWithoutDocumentation()
"
				"" Update the highlighting whenever leaving insert mode
				"autocmd InsertLeave *.cs call OmniSharp#HighlightBuffer()
"
				"" Alternatively, use a mapping to refresh highlighting for the current buffer
				"autocmd FileType cs nnoremap <buffer> <Leader>th :OmniSharpHighlightTypes<CR>
"
				"" The following commands are contextual, based on the cursor position.
				"autocmd FileType cs nnoremap <buffer> gd :OmniSharpGotoDefinition<CR>
				"autocmd FileType cs nnoremap <buffer> <Leader>fi :OmniSharpFindImplementations<CR>
				"autocmd FileType cs nnoremap <buffer> <Leader>fs :OmniSharpFindSymbol<CR>
				"autocmd FileType cs nnoremap <buffer> <Leader>fu :OmniSharpFindUsages<CR>
"
				"" Finds members in the current buffer
				"autocmd FileType cs nnoremap <buffer> <Leader>fm :OmniSharpFindMembers<CR>
"
				"autocmd FileType cs nnoremap <buffer> <Leader>fx :OmniSharpFixUsings<CR>
				"autocmd FileType cs nnoremap <buffer> <Leader>tt :OmniSharpTypeLookup<CR>
				"autocmd FileType cs nnoremap <buffer> <Leader>dc :OmniSharpDocumentation<CR>
				"autocmd FileType cs nnoremap <buffer> <C-\> :OmniSharpSignatureHelp<CR>
				"autocmd FileType cs inoremap <buffer> <C-\> <C-o>:OmniSharpSignatureHelp<CR>
"
				"" Navigate up and down by method/property/field
				"autocmd FileType cs nnoremap <buffer> <M-k> :OmniSharpNavigateUp<CR>
				"autocmd FileType cs nnoremap <buffer> <M-j> :OmniSharpNavigateDown<CR>
			"augroup END
"
			"" Contextual code actions (uses fzf, CtrlP or unite.vim when available)
			"autocmd FileType cs nnoremap <Leader>; :OmniSharpGetCodeActions<CR>
			"" Run code actions with text selected in visual mode to extract method
			"autocmd FileType cs xnoremap <Leader><Space> :call OmniSharp#GetCodeActions('visual')<CR>
"
			"" Rename with dialog
			"autocmd FileType cs nnoremap <Leader>rn :OmniSharpRename<CR>
			""autocmd FileType cs nnoremap <F2> :OmniSharpRename<CR>
			"" Rename without dialog - with cursor on the symbol to rename: `:Rename newname`
			"autocmd FileType cs command! -nargs=1 Rename :call OmniSharp#RenameTo("<args>")
"
			"autocmd FileType cs nnoremap <Leader>cf :OmniSharpCodeFormat<CR>
"
			"" Start the omnisharp server for the current solution
			"autocmd FileType cs nnoremap <Leader>ss :OmniSharpStartServer<CR>
			"autocmd FileType cs nnoremap <Leader>sp :OmniSharpStopServer<CR>
"
			"" Enable snippet completion
			"" let g:OmniSharp_want_snippet=1
"
		"" Stuff I want specifically:
			"autocmd FileType cs call OmniSharp#HighlightBuffer()
			"autocmd FileType cs inoremap <C-Space> <C-x><C-o>
"
		"" Compiling and executing:
			"autocmd FileType cs nnoremap <F5> <Esc>:!mcs<Space>%:p<Space>&&<Space>echo<Space>Compiling<Space>complete.<Enter>
			"autocmd FileType cs nnoremap <F6> <Esc>:!mono<Space>%:r.exe<Enter>
"
"
		"" Template generation:
			""autocmd FileType cs inoremap ;template using<Space>System;<Enter><++><Enter><Enter>namespace<Space><++><Space>{<Enter>//<Space><++><Enter><Esc>^xxiclass<Space><++><Space>{<Enter>static<Space>void<Space>Main(string[]<Space>args)<Space>{<Enter>//<Space><++><Enter><++><Esc>?<++><Enter>5n"_c4l
"
		"" Convenient commands:
			""autocmd FileType cs inoremap ;cwl Console.WriteLine(<++>);<++><Esc>?<++><Enter>n"_c4l
			""autocmd FileType cs inoremap ;cwr Console.Write("<++>");<Enter><++><Space>=<Space>Console.ReadLine();<Enter><++><Esc>?<++><Enter>2n"_c4l
		"" Wiggly brackets {These ones}:
			"autocmd FileType cs inoremap {<Enter> {<Enter>}<++><Esc>O
		"autocmd FileType cs inoremap { {<++><Enter>}<++><Esc>?<++><Enter>n:nohlsearch<Enter>"_c4l

	" Normal Markdown:
	    "autocmd FileType markdown map <F4> :!echo<space>
	    "autocmd FileType markdown map <F5> :!pandoc<space>-V<space>geometry:margin=2cm<space>-o<space>%:r.pdf<space>%:p<enter><enter>
	    "autocmd FileType markdown map <F6> :!pandoc<space>%:p<space>-V<space>aspectratio:169<space>-t<space>beamer<space>-o<space>%:r.pdf<enter><enter>
	    autocmd FileType markdown map <F7> :terminal ++close /home/aidan/scripts/compile3.sh %:p <CR><CR>
	    autocmd FileType markdown map <F8> :!zathura<space>%:r.pdf<space>&<enter><enter>

	    let g:vim_markdown_new_list_item_indent = 0


	" R Markdown:
		autocmd FileType rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>

	" Rust:
	    if executable('rls')
		au User lsp_setup call lsp#register_server({
		    \ 'name': 'rls',
		    \ 'cmd': {server_info->['rustup', 'run', 'nightly', 'rls']},
		    \ 'whitelist': ['rust'],
		    \ })
	    endif 


	    set hidden
	    let g:racer_cmd = "/home/aidan/.cargo/bin/racer"
	    let g:racer_insert_paren = 1
	    let g:racer_experimental_completer = 1

	    au FileType rust nmap <leader>gD <Plug>(rust-def)
	    au FileType rust nmap <leader>gs <Plug>(rust-def-split)
	    au FileType rust nmap <leader>gx <Plug>(rust-def-vertical)
	    au FileType rust nmap <leader>gd <Plug>(rust-doc)
	    "autocmd FileType rust map <F5> :terminal<enter>cargo<space>build<enter>
	    "autocmd FileType rust map <F6> :terminal<enter>cargo<space>run<enter>
	    autocmd FileType rust map <F7> :terminal<enter>cargo<space>check<enter>
	    autocmd FileType rust map <F8> :terminal<enter>cargo<space>build<space>--release<enter>

	    " Special settings:

	    autocmd FileType rust set termwinsize=20x0

	" Groff:
	    autocmd FileType ms filetype detect
	    autocmd FileType nroff filetype detect
	    autocmd FileType nroff map <F8> :!zathura<space>%:r.pdf<space>&<enter><enter>
	    autocmd FileType nroff set softtabstop=0 shiftwidth=0 noautoindent nocindent
	" Java:
	    autocmd filetype java setlocal omnifunc=javacomplete#Complete

	    " Asyncomplete Stuff:
		"if executable('java') && filereadable(expand('~/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.300.v20190213-1655.jar'))
		        "au User lsp_setup call lsp#register_server({
			        "\ 'name': 'eclipse.jdt.ls',
			        "\ 'cmd': {server_info->[
			        "\     'java',
			        "\     '-Declipse.application=org.eclipse.jdt.ls.core.id1',
			        "\     '-Dosgi.bundles.defaultStartLevel=4',
			        "\     '-Declipse.product=org.eclipse.jdt.ls.core.product',
			        "\     '-Dlog.level=ALL',
			        "\     '-noverify',
			        "\     '-Dfile.encoding=UTF-8',
			        "\     '-Xmx1G',
			        "\     '-jar',
			        "\     expand('~/lsp/eclipse.jdt.ls/plugins/org.eclipse.equinox.launcher_1.5.300.v20190213-1655.jar'),
			        "\     '-configuration',
			        "\     expand('~/lsp/eclipse.jdt.ls/config_linux'),
			        "\     '-data',
			        "\     getcwd()
			        "\ ]},
			        "\ 'whitelist': ['java'],
			        "\ })
		"endif

" Overrides:
    " These are put at the end of the .vimrc in the hope that they are used.
    
    set completeopt=menu,preview
