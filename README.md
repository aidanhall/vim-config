# Nice.

This is just so I can more easily transfer my vim config to different machines.
It might be useful to create multiple versions at some point, but I don't think that it's likely.

## Usage:

1. Copy .vimrc and .vim to your home directory.
1. Start Vim and run: ```:PlugInstall```
