" Syntax highlighting:
	highlight Normal guifg=white guibg=gray6
	highlight LineNr guibg=gray20 guifg=crimson
	highlight Comment guifg=red gui=italic
	highlight Constant guifg=magenta
	highlight Identifier guifg=MediumSlateBlue
	highlight Statement guifg=yellow
	highlight Type guifg=green
	highlight Error guibg=red guifg=black gui=bold
	highlight Special guifg=red

	" C#:
		highlight csModifier guifg=DeepSkyBlue1
		highlight csUserType guifg=goldenrod1
		highlight csUserMethod guifg=chartreuse2
		highlight csNumber guifg=DeepPink
		"highlight csUserIdentifier guifg=turquoise1
		highlight csStorage guifg=MediumOrchid1
		highlight csComment guifg=DarkSeaGreen1 gui=italic
		highlight csBraces guifg=DarkOliveGreen1

" Changing working directory to that of the file opened
" to make it easier to open more files in that directory.
cd %:p:h

" Copy and paste:
	vnoremap <C-c> "*y :let @+=@*<CR>
	map <C-p> "+gP
